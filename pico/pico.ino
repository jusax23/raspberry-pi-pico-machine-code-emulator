#define pram_length 65536
#define program_length 65536

//#define s_debug

byte pram[pram_length];
byte prog[program_length];

uint16_t pcounter = 0;
uint16_t pstackpointer = pram_length-1;
uint16_t pframepointer = pram_length-1;
uint16_t pstackmax = pram_length*0.5;

uint32_t AReg;
uint32_t BReg;
uint32_t CReg;

uint32_t XReg;

bool equalFlag;
bool biggerFlag;
bool smalerFlag;

bool doExecute = false;

void setup() {
  Serial.begin(115200);
  pinMode(25, OUTPUT);
}

String b16 = "0123456789abcdef";


void loop() {
  while (true){
    while(Serial.available()){
      String c = Serial.readStringUntil('\n');
      if (c == "start")doExecute = true;
      else if (c == "stop")doExecute = false;
      else if (c == "test"){
        for(int i = 0; i < 115; i++) {
          Serial.println(prog[i]);
        }
        Serial.println(pcounter);
      }
      else if (c == "clear"){
        pcounter = 0;
        pstackpointer = pram_length-1;
        pstackmax = pram_length*0.5;
        AReg = 0;
        BReg = 0;
        CReg = 0;
        XReg = 0;
        equalFlag = false;
        biggerFlag = false;
        smalerFlag = false;
        for (int i = 0; i < pram_length; i++) {
          pram[i] = 0;
        }
      }
      else if (c=="dump"){
        for(int j = 0; j < 4; j++){
          for(int i = 0; i < 16; i++){
            Serial.print(b16[pram[i]>>4]);
            Serial.print(b16[pram[i]&0xf]);
            Serial.print(",");
          }
          Serial.println();
        }

      }
      else{
        for (int i = 0; i < c.length()-1; i=i+2) {
          prog[i/2] = b16.indexOf(c[i])*16+b16.indexOf(c[i+1]);
        }
      }
    }
    if(doExecute){
      digitalWrite(25, true);
      #ifdef s_debug
        Serial.print("Line: ");
        Serial.print(pcounter);
        Serial.print(" > ");
        Serial.print(prog[pcounter]);
        Serial.print(" > ");
        Serial.print(pram[65528]);
        Serial.print(" > ");
        Serial.print(pstackpointer);
        Serial.println(" ... ");
      #endif
      execute();
      #ifdef s_debug
        Serial.print(" ... ");
        Serial.print(pram[65528]);
        Serial.print(" > ");
        Serial.println("done");
      #endif
    }else{
      digitalWrite(25, false);
    }
  }


  //delay(1);
}

void execute(){
  byte cmd = prog[pcounter++];
  switch ((int)cmd){
    case 0:{
      __asm__("nop");
    }break;
    case 1:{ // LDA
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data = local?pframepointer-data:data;
      AReg = 0;
      for (byte i = 0; i < n; i++) {
        AReg += pram[data+i*(local?-1:1)]<<(i*8);
      }
    }break;
    case 2:{ // LDB
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data = local?pframepointer-data:data;
      BReg = 0;
      for (byte i = 0; i < n; i++) {
        BReg += pram[data+i*(local?-1:1)]<<(i*8);
      }
    }break;
    case 3:{ // LDC
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data = local?pframepointer-data:data;
      CReg = 0;
      for (byte i = 0; i < n; i++) {
        CReg += pram[data+i*(local?-1:1)]<<(i*8);
      }
    }break;
    case 4:{ // LDX
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data = local?pframepointer-data:data;
      XReg = 0;
      for (byte i = 0; i < n; i++) {
        XReg += pram[data+i*(local?-1:1)]<<(i*8);
      }
    }break;

    case 5:{ // LOA
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data += XReg;//*n;
      data = local?pframepointer-data:data;
      AReg = 0;
      for (byte i = 0; i < n; i++) {
        AReg += pram[data+i*(local?-1:1)]<<(i*8);
      }
    }break;
    case 6:{ // LOB
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data += XReg;//*n;
      data = local?pframepointer-data:data;
      BReg = 0;
      for (byte i = 0; i < n; i++) {
        BReg += pram[data+i*(local?-1:1)]<<(i*8);
      }
    }break;
    case 7:{ // LOC
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data += XReg;//*n;
      data = local?pframepointer-data:data;
      CReg = 0;
      for (byte i = 0; i < n; i++) {
        CReg += pram[data+i*(local?-1:1)]<<(i*8);
      }
    }break;

    case 8:{ // STA
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data = local?pframepointer-data:data;
      for (byte i = 0; i < n; i++) {
        pram[data+i*(local?-1:1)] = (AReg>>(i*8))&0xff;
      }
    }break;
    case 9:{ // STB
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data = local?pframepointer-data:data;
      for (byte i = 0; i < n; i++) {
        pram[data+i*(local?-1:1)] = (BReg>>(i*8))&0xff;
      }
    }break;
    case 10:{ // STC
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data = local?pframepointer-data:data;
      for (byte i = 0; i < n; i++) {
        pram[data+i*(local?-1:1)] = (CReg>>(i*8))&0xff;
      }
    }break;
    case 11:{ // STX
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data = local?pframepointer-data:data;
      for (byte i = 0; i < n; i++) {
        pram[data+i*(local?-1:1)] = (XReg>>(i*8))&0xff;
      }
    }break;

    case 12:{ // SOA
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data += XReg;//*n;
      data = local?pframepointer-data:data;
      for (byte i = 0; i < n; i++) {
        pram[data+i*(local?-1:1)] = (AReg>>(i*8))&0xff;
      }
    }break;
    case 13:{ // SOB
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data += XReg;//*n;
      data = local?pframepointer-data:data;
      for (byte i = 0; i < n; i++) {
        pram[data+i*(local?-1:1)] = (BReg>>(i*8))&0xff;
      }
    }break;
    case 14:{ // SOC
      byte s = prog[pcounter++];
      byte n = min(s&0xf,4);
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      boolean local = ((s&0xf0)!=0);
      data += XReg;//*n;
      data = local?pframepointer-data:data;
      for (byte i = 0; i < n; i++) {
        pram[data+i*(local?-1:1)] = (CReg>>(i*8))&0xff;
      }
    }break;

    case 15:{ // LIA
      uint32_t data = prog[pcounter++]<<24;
      data += prog[pcounter++]<<16;
      data += prog[pcounter++]<<8;
      data += prog[pcounter++];
      AReg = data;
    }break;
    case 16:{ // LIB
      uint32_t data = prog[pcounter++]<<24;
      data += prog[pcounter++]<<16;
      data += prog[pcounter++]<<8;
      data += prog[pcounter++];
      BReg = data;
    }break;
    case 17:{ // LIC
      uint32_t data = prog[pcounter++]<<24;
      data += prog[pcounter++]<<16;
      data += prog[pcounter++]<<8;
      data += prog[pcounter++];
      CReg = data;
    }break;
    case 18:{ // LIX
      uint32_t data = prog[pcounter++]<<24;
      data += prog[pcounter++]<<16;
      data += prog[pcounter++]<<8;
      data += prog[pcounter++];
      XReg = data;
    }break;

    case 19:{ // CAB
      BReg = AReg;
    }break;
    case 20:{ // CBC
      CReg = BReg;
    }break;
    case 21:{ // CCA
      AReg = CReg;
    }break;
    case 22:{ // CAC
      CReg = AReg;
    }break;
    case 23:{ // CCB
      BReg = CReg;
    }break;
    case 24:{ // CBA
      AReg = BReg;
    }break;

    case 25:{ // CAX
      XReg = AReg;
    }break;
    case 26:{ // CBX
      XReg = BReg;
    }break;
    case 27:{ // CCX
      XReg = CReg;
    }break;
    case 28:{ // CXA
      AReg = XReg;
    }break;
    case 29:{ // CXB
      BReg = XReg;
    }break;
    case 30:{ // CXC
      CReg = XReg;
    }break;

    //Types unsigned 0: uint;
    //        signed 1: int; 2: float

    case 31: { //CVA
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];
      if(data2 == 2){
        float o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 2){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        AReg = * (uint32_t * ) &o;
      }else if(data2 == 1){
        int32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        AReg = * (uint32_t * ) &o;
      }else {
        uint32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        AReg = * (uint32_t * ) &o;
      }
    }break;
    case 32:{ //CVB
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];
      if(data2 == 2){
        float o = 0;
        if(data1 == 1){o= * ( int32_t * ) &BReg;}
        else if(data1 == 0){o=BReg;}
        else if(data1 == 2){o= * ( float * ) &BReg;}
        BReg = * (uint32_t * ) &o;
      }else if(data2 == 1){
        int32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &BReg;}
        else if(data1 == 0){o=BReg;}
        else if(data1 == 2){o= * ( float * ) &BReg;}
        BReg = * (uint32_t * ) &o;
      }else {
        uint32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &BReg;}
        else if(data1 == 0){o=BReg;}
        else if(data1 == 2){o= * ( float * ) &BReg;}
        BReg = * (uint32_t * ) &o;
      }
    }break;
    case 33:{ //CVC
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];
      if(data2 == 2){
        float o = 0;
        if(data1 == 1){o= * ( int32_t * ) &CReg;}
        else if(data1 == 0){o=CReg;}
        else if(data1 == 2){o= * ( float * ) &CReg;}
        CReg = * (uint32_t * ) &o;
      }else if(data2 == 1){
        int32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &CReg;}
        else if(data1 == 0){o=CReg;}
        else if(data1 == 2){o= * ( float * ) &CReg;}
        CReg = * (uint32_t * ) &o;
      }else {
        uint32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &CReg;}
        else if(data1 == 0){o=CReg;}
        else if(data1 == 2){o= * ( float * ) &CReg;}
        CReg = * (uint32_t * ) &o;
      }
    }break;


    case 34:{ // ADD
      byte data = prog[pcounter++];
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];

      if(data == 2){
        float o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o+= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o+=BReg;}
        else if(data2 == 2){o+= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }else if(data == 1){
        int32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o+= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o+=BReg;}
        else if(data2 == 2){o+= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }else {
        uint32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o+= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o+=BReg;}
        else if(data2 == 2){o+= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }
    }break;
    case 35:{ // SUB
      byte data = prog[pcounter++];
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];

      if(data == 2){
        float o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o-= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o-=BReg;}
        else if(data2 == 2){o-= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }else if(data == 1){
        int32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o-= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o-=BReg;}
        else if(data2 == 2){o-= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }else {
        uint32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o-= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o-=BReg;}
        else if(data2 == 2){o-= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }
    }break;
    case 36:{ //MUL
      byte data = prog[pcounter++];
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];

      if(data == 2){
        float o = 1;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o*= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o*=BReg;}
        else if(data2 == 2){o*= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }else if(data == 1){
        int32_t o = 1;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o*= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o*=BReg;}
        else if(data2 == 2){o*= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }else {
        uint32_t o = 1;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o*= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o*=BReg;}
        else if(data2 == 2){o*= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }
    }break;
    case 37:{ //DIV
      byte data = prog[pcounter++];
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];

      if(data == 2){
        float o = 1;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o/= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o/=BReg;}
        else if(data2 == 2){o/= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }else if(data == 1){
        int32_t o = 1;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o/= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o/=BReg;}
        else if(data2 == 2){o/= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }else{
        uint32_t o = 1;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o/= * ( int32_t * ) &BReg;}
        else if(data2 == 0){o/=BReg;}
        else if(data2 == 2){o/= * ( float * ) &BReg;}
        AReg = * (uint32_t * ) &o;
      }
    }break;
    case 38:{ //MOD
      byte data = prog[pcounter++];
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];

      if(data == 2){
        uint32_t o = 1;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        if(data2 == 1){o = o % (* ( int32_t * ) &BReg);}
        else if(data2 == 0){o=o%BReg;}
        AReg = * (uint32_t * ) &o;
      }else{
        int32_t o = 1;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        if(data2 == 1){o = o % (* ( int32_t * ) &BReg);}
        else if(data2 == 0){o=o%BReg;}
        AReg = * (uint32_t * ) &o;
      }
    }break;

    case 39:{ // POW
      byte data = prog[pcounter++];
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];

      if(data == 2){
        float o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o=pow(o, * ( int32_t * ) &BReg);}
        else if(data2 == 0){o=pow(o, BReg);}
        else if(data2 == 2){o=pow(o, * ( float * ) &BReg);}
        AReg = * (uint32_t * ) &o;
      }else if(data == 1){
        int32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o=pow(o, * ( int32_t * ) &BReg);}
        else if(data2 == 0){o=pow(o, BReg);}
        else if(data2 == 2){o=pow(o, * ( float * ) &BReg);}
        AReg = * (uint32_t * ) &o;
      }else {
        uint32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o=pow(o, * ( int32_t * ) &BReg);}
        else if(data2 == 0){o=pow(o, BReg);}
        else if(data2 == 2){o=pow(o, * ( float * ) &BReg);}
        AReg = * (uint32_t * ) &o;
      }
    }break;
    case 40:{ // ROT
      byte data = prog[pcounter++];
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];

      if(data == 2){
        float o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o=pow(o,1.0/ (* ( int32_t * ) &BReg));}
        else if(data2 == 0){o=pow(o, 1.0/BReg);}
        else if(data2 == 2){o=pow(o,1.0/( * ( float * ) &BReg));}
        AReg = * (uint32_t * ) &o;
      }else if(data == 1){
        int32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o=pow(o,1.0/ (* ( int32_t * ) &BReg));}
        else if(data2 == 0){o=pow(o, 1.0/BReg);}
        else if(data2 == 2){o=pow(o,1.0/( * ( float * ) &BReg));}
        AReg = * (uint32_t * ) &o;
      }else {
        uint32_t o = 0;
        if(data1 == 1){o= * ( int32_t * ) &AReg;}
        else if(data1 == 0){o=AReg;}
        else if(data1 == 2){o= * ( float * ) &AReg;}
        if(data2 == 1){o=pow(o, * ( int32_t * ) &BReg);}
        else if(data2 == 0){o=pow(o, BReg);}
        else if(data2 == 2){o=pow(o, * ( float * ) &BReg);}
        AReg = * (uint32_t * ) &o;
      }
    }break;
    case 41:{ // LOG
      byte data = prog[pcounter++];
      byte data1 = prog[pcounter++];

      if(data == 2){
        float o = 0;
        if(data1 == 1){o=log( * ( int32_t * ) &AReg);}
        else if(data1 == 0){o=log(AReg);}
        else if(data1 == 2){o= log(* ( float * ) &AReg);}
        AReg = * (uint32_t * ) &o;
      }else if(data == 1){
        int32_t o = 0;
        if(data1 == 1){o=log( * ( int32_t * ) &AReg);}
        else if(data1 == 0){o=log(AReg);}
        else if(data1 == 2){o= log(* ( float * ) &AReg);}
        AReg = * (uint32_t * ) &o;
      }else {
        uint32_t o = 0;
        if(data1 == 1){o=log( * ( int32_t * ) &AReg);}
        else if(data1 == 0){o=log(AReg);}
        else if(data1 == 2){o= log(* ( float * ) &AReg);}
        AReg = * (uint32_t * ) &o;
      }
    }break;

    case 42:{ // SR
      AReg = AReg >> BReg;
    }break;
    case 43:{ // SL
      AReg = AReg << BReg;
    }break;

    case 44:{ // INC
      byte data1 = prog[pcounter++];
      if(data1==0||data1==1){
        AReg++;
      }else if(data1==2){
        float o = * (float *) &AReg;
        o++;
        AReg = * (uint32_t * ) &o;
      }

    }break;
    case 45:{ // DEC
      byte data1 = prog[pcounter++];
      if(data1==0||data1==1){
        AReg--;
      }else if(data1==2){
        float o = * (float *) &AReg;
        o--;
        AReg = * (uint32_t * ) &o;
      }
    }break;

    case 46:{ // BWO
      AReg = AReg | BReg;
    }break;
    case 47:{ // BWA
      AReg = AReg & BReg;
    }break;
    case 48:{ // BWX
      AReg = AReg ^ BReg;
    }break;

    case 49:{ //CMP
      byte data1 = prog[pcounter++];
      byte data2 = prog[pcounter++];

      double o = 0;
      if(data1 == 1){o= * ( int32_t * ) &AReg;}
      else if(data1 == 0){o=AReg;}
      else if(data1 == 2){o= * ( float * ) &AReg;}
      if(data2 == 1){o-= * ( int32_t * ) &BReg;}
      else if(data2 == 0){o-=BReg;}
      else if(data2 == 2){o-= * ( float * ) &BReg;}
      equalFlag = (fabs(o) < 0.00005f);
      biggerFlag = (o >= 0.00005f);
      smalerFlag = (o <= -0.00005f);
    }break;
    case 50:{ //JMP
      uint16_t data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      pcounter = data;
    }break;
    case 51:{ //JE
      uint16_t data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      if(equalFlag){
        pcounter = data;
      }
    }break;
    case 52:{ //JNE
      uint16_t data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      if(!equalFlag){
        pcounter = data;
      }
    }break;
    case 53:{ //JB
      uint16_t data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      if(biggerFlag){
        pcounter = data;
      }
    }break;
    case 54:{ //JNB
      uint16_t data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      if(!biggerFlag){
        pcounter = data;
      }
    }break;
    case 55:{ //JS
      uint16_t data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      if(smalerFlag){
        pcounter = data;
      }
    }break;
    case 56:{ //JNS
      uint16_t data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      if(!smalerFlag){
        pcounter = data;
      }
    }break;

    case 57:{ //PSH
      int data = max(min(prog[pcounter++],255),1);
      for (int i = data-1; i >= 0; i--) {
        if(i<=4){
          pram[pstackpointer--] = (AReg >> (i*8))&0xff;
        }else{
          pram[pstackpointer--] = 0;
        }
      }
      if(pram_length-pstackpointer>pstackmax){
        doExecute = false;
        Serial.println("Stackoverflow: PSH");
      }
    }break;
    case 58:{ //PUL
      int data = max(min(prog[pcounter++]+0,255),1);
      AReg = 0;
      for (int i = 0; i < data; i++) {
        if(i<=4){
          AReg+=pram[++pstackpointer]<<(i*8);
        }else{
          pstackpointer++;
        }
      }
      if(pram_length-pstackpointer>pstackmax){
        doExecute = false;
        Serial.println("Stackunderflow: PUL");
      }
    }break;
    case 59:{ //JSR
      uint16_t data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      pram[pstackpointer--] = (pframepointer)&0xff;
      pram[pstackpointer--] = (pframepointer>>8)&0xff;
      pram[pstackpointer--] = (pcounter)&0xff;
      pram[pstackpointer--] = (pcounter>>8)&0xff;
      pframepointer = pstackpointer;
      if(pram_length-pstackpointer>pstackmax){
        doExecute = false;
        Serial.println("Stackoverflow: JSR");
      }
      pcounter = data;
    }break;
    case 60:{ //JSA
      uint16_t data = AReg;
      pram[pstackpointer--] = (pframepointer)&0xff;
      pram[pstackpointer--] = (pframepointer>>8)&0xff;
      pram[pstackpointer--] = (pcounter)&0xff;
      pram[pstackpointer--] = (pcounter>>8)&0xff;
      pframepointer = pstackpointer;
      if(pram_length-pstackpointer>pstackmax){
        doExecute = false;
        Serial.println("Stackoverflow: JSA");
      }
      pcounter = data;
    }break;
    case 61:{ //RSR
      pcounter = 0;
      pstackpointer = pframepointer;
      pcounter = pram[++pstackpointer]<<8;
      pcounter += pram[++pstackpointer];
      pframepointer = pram[++pstackpointer]<<8;
      pframepointer += pram[++pstackpointer];
      if(pram_length-pstackpointer>pstackmax){
        doExecute = false;
        Serial.println("Stackunderflow: RSR");
      }
    }break;


    case 252:{ //IN

    }break;
    case 253:{ //OUT
      byte data = prog[pcounter++];
      if(data==1) Serial.println(* (int *) &AReg);
      if(data==0) Serial.println(AReg);
      if(data==2) Serial.println(* (float *) &AReg);
    }break;
    case 254:{ //SHS Set Heap Size
      unsigned short data = prog[pcounter++]<<8;
      data += prog[pcounter++];
      pstackmax = pram_length-data;
      if(pram_length-pstackpointer>pstackmax){
        doExecute = false;
        Serial.println("Stackoverflow: SHS");
      }
    }break;
    case 255:{ //HLT    bzw. exit
      pcounter = 0;
      doExecute = false;
    }break;
  }
}
