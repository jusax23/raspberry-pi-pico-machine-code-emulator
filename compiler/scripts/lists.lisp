(defvar x:*uint16 (list 3 2 1))

(print (list-nth x 0))
(print (list-nth x 1))
(print (list-nth x 2))

(list-set x 1 5)

(print (list-nth x 0))
(print (list-nth x 1))
(print (list-nth x 2))
