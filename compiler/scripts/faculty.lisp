(defun faculty:uint32 (input:uint32)
  (if (< input 2) 1 (* input (faculty (- input 1))))
)
(print (faculty 10))
