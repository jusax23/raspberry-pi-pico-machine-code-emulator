if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]
  then
    echo "Use command with folloing Arguments: sh build.sh [lisp-file-input] [assembly-file-output] [hex-file-output]"
    exit 1
fi

echo "Compiling to Assembly: "
node tools/lispToAs.js $1 $2 || exit 1
echo "Compiling to Binary: "
node tools/asToBinary.js $2 $3 || exit 1
echo "done"
