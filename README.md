# Raspberry Pi Pico Machine Code Emulator with compiler for LISP

Recently I was wondering if it is possible to run a program from the pico's ram. That's my answer!

## Try it?

- Download the git repository.
- Flash pico.ino with the arduino ide.
- Write a program with my LISP Syntax
- Compile it with: sh build.sh [lisp-file-input] [assembly-file-output] [hex-file-output]
- Copy and Past the Binary in Hex in the Serial Monitor.
- Type start to run your Program.

## Syntax in LISP

### DataTypes

- uint8
- uint16
- uint32
- int8
- int16
- int32
- float
- bool
- (Strings or chars are currently not supported)

### Calculations
```lisp
(+ a b)   
(+ a b c) ;...

(- a b)
(- a b c) ;...

(* a b)   
(* a b c) ;...

(/ a b)
(/ a b c) ;...
```
### print

```lisp
(print a)
```

### Comparisons and Branches

```lisp
(< 4 5)
(> 5 4)
(= 6 6)

(if (< 4 5) (print 1) (print 0))
;if [condition] [then] [else]
```

### Variables
Define Variables:
```lisp
(defvar myvar1:uint32 4)
(defvar myvar2:float (+ 5 1.5))
```
Set Variables:
```lisp
(let myvar1 4)
(let myvar2 (+ 5 1))
```

### Functions
Define and call Functions:

```lisp
(defun faculty:uint32 (input:uint32)
  (print input)
  (if (< in 2) 1 (* input (faculty (- input 1))))
)
(print (faculty 10))
```
